<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Resources\CarCollection;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

/**
 * TODO: response codes, follow correct API specification
 */
class CarsController extends Controller
{
    public function index(): CarCollection
    {
        return new CarCollection(Car::currentUser()->get());
    }

    public function store(Request $request): JsonResponse
    {
        /**
         * @note Use tommorow->year to account for Austalian time zone in 31 dec
         */
        $request->validate([
            'year' => 'required|digits:4|integer|min:1901|max:'.Carbon::tomorrow()->year,
            'make' => 'required|string',
            'model' => 'required|string'
        ]);
        $car = new Car();
        $car->fill($request->json()->all());
        $car->user = Auth::id();
        $car->save();
        return response()->json([]);
    }

    public function show(int $id): JsonResponse
    {
        $car = Car::idEquals($id)
            ->withTripMiles()
            ->withTripCount()
            ->first();
        $this->authorize('owner', $car);
        if (is_null($car)) {
            return response()->json('Data not found', 404);
        }
        return response()->json(['data' => $car]);
    }

    public function destroy(Car $car): JsonResponse
    {
        $this->authorize('owner', $car);
        $car->delete();

        return response()->json([]);
    }
}
