<?php

namespace App\Http\Controllers;

use App\Http\Resources\TripCollection;
use App\Trip;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TripsController extends Controller
{
    public function index(): TripCollection
    {
        return new TripCollection(Trip::currentUser()->get());
    }

    public function store(Request $request): JsonResponse
    {
        /**
         * @note Use tommorow->year to account for Austalian time zone in 31 dec
         */
        $request->validate([
            'date' => 'required|date',
            'car_id' => 'required|integer',
            'miles' => 'required|numeric'
        ]);
        $trip = new Trip();
        $trip->date = Carbon::parse($request->date)->format('Y-m-d H:i:s');
        $trip->car = $request->car_id;
        $trip->miles = $request->miles;
        $trip->miles_balance = $trip->getMilesForCar($trip->car) + $trip->miles;
        $trip->save();
        return response()->json([]);
    }
}
