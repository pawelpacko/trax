<?php

namespace App\Policies;

use App\Car;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CarsPolicy
{
    use HandlesAuthorization;

    public function owner(User $user, Car $car): bool
    {
        return $user->id === $car->user;
    }
}
