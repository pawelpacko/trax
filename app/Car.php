<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

class Car extends Model
{
    protected $fillable = [
        'year', 'make', 'model',
    ];

    public function scopeCurrentUser(Builder $query): Builder
    {
        return $query->where('user', '=', auth('api')->user()->getAuthIdentifier());
    }

    public function scopeIdEquals(Builder $query, int $id): Builder
    {
        return $query->where('id', '=', $id);
    }

    public function scopeWithTripMiles(Builder $query): Builder
    {
        /**
         * @todo Laravel 7 doesn't support Model::withSum(). Refactor to withSum with update
         * @note COALESCE is required, otherwise sum(miles) may return null
         */
        return $query->withCount(['trips AS trip_miles' => function ($query) {
            $query->select(DB::raw('COALESCE(SUM(miles),0)'));
        }]);
    }

    public function scopeWithTripCount(Builder $query): Builder
    {
        return $query->withCount(['trips AS trip_count']);
    }

    /**
     * @TODO: remove maybe? check with test
     */
    public function trips(): HasMany
    {
        return $this->hasMany(Trip::class, 'car');
    }


}
