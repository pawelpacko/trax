<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Trip extends Model
{
    /**
     * @note CurrentUser enforces Car table
     */
    public function scopeCurrentUser(Builder $query): Builder
    {
        return $query
            ->with('car')
            ->whereHas('car', function ($query) {
                $query->where('cars.user', '=', auth('api')->user()->getAuthIdentifier());
            })
            ->select(['id','date','miles','miles_balance as total','car']);
    }

    public function car(): BelongsTo
    {
        return $this->belongsTo(Car::class, 'car');
    }

    public function getMilesForCar(int $carId): float
    {
        return self::query()
            ->where('car', '=', $carId)
            ->sum('miles');
    }
}
