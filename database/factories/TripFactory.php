<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Trip::class, function (Faker $faker) {
    return [
        'date' => $faker->date(),
        'miles' => $faker->randomFloat(4,0, 500),
        'miles_balance' => $faker->randomFloat(4, 500, 1000),
        'user' => 1
    ];
});
