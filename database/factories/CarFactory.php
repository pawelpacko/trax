<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Car::class, function (Faker $faker) {
    return [
        'year' => $faker->year,
        'make' => $faker->city,
        'model' => $faker->streetName,
        'user' => 1
    ];
});
